# bun-cn

Bun 是一个超快的 JavaScript/TypeScript 运行时，和 Node.js 兼容。

bun-cn 是帮助中国开发者，更好地使用 bun 的项目，包括在通过中国镜像源来加速安装 Bun 的脚本，和翻译校对后的 Bun 的中文文档。

> 注意：bun-cn 是由 Bun 的忠实爱好者自发建立的仓库，与 Bun 官方没有任何关系。

## 安装

使用如下命令，可以安装最新版本的 Bun。会自动使用国内访问速度较好的源进行安装。

> 注意：如果你使用 Windows，由于 Bun 对 Windows 的支持尚不完善，请使用 WSL + VS Code 进行开发，可以享受无缝的开发体验。参考文档：[在 WSL 中开发](https://code.visualstudio.com/docs/remote/wsl)

```sh
# 本脚本只对 Linux、Mac、WSL2 有效，Windows 请使用 WSL + VS Code 进行开发
curl -fsSL https://gitee.com/akirarika/bun-cn/raw/main/install.sh | bash
```

本脚本只对 Linux、Mac、WSL2 有效，Windows 请使用 WSL + VS Code 进行开发

## 升级

如果你想将 Bun 升级至最新版本，重新运行上方的安装命令即可。如果使用 `bun upgrade`，将使用官方提供的源进行升级，在国内可能速度不佳。

## 切换指定版本的 bun

```bash
curl -fsSL https://gitee.com/akirarika/bun-cn/raw/main/install.sh | bash -s 1.0.0 # 将切换至 1.0.0 版本
```

## 脚本做了什么

Bun 的官方脚本从 Github 上下载，但实际上，Bun 除了将二进制文件发布在 GitHub 之外，还发布到了 npm 上。

因此，我们就可以使用国内的 npm 镜像源来以更快的速度安装 Bun。

在安装结束之后，如果你不存在 `~/bunfig.toml` 文件，脚本还会为你自动创建一个，并设置为使用国内镜像源。

## 中文文档

bun-cn 计划使用 Gitee Pages 来提供经过翻译校对的中文文档，但尚未开始。如果你对这方面有经验并且想提供帮助的话，欢迎联系我～

## 中国 bun 开源项目

- [Milkio](https://zh-milkio.nito.ink/) - 超快 bun 后端框架，打破前后端的界限

如果你开源了使用 Bun 的项目，欢迎提交 PR，来使自己的项目在这里得到展示～

此处只展示面向开发者的项目，如框架或者类库。如果你使用 Bun 制作了应用或网站，请在[这里](./POWERD_BY_BUN.md)展示。